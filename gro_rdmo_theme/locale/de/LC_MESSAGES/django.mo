��    $      <  5   \      0  �   1  �   �  �  �     e     k     |     �     �     �     �     �  #   �  #     .   )     X     `     m  
   ~  X  �  8   �     	     #	     4	  >   A	     �	     �	     �	     �	  *   �	     �	     
      
     5
     S
     `
  "  o
  `   �  �   �  	  �     �     �     �     �     �               2  &   H      o  (   �  	   �     �     �  
   �  9  �  8   0     i     |     �  <   �     �     �     �       0   +     \     p     �  #   �     �     �     
                                                                  $                                   !         #                             "                                	    
            To create a Data Management Plan (DMP) for this project, you need to fill in the project questionnaire. Click below to start this process.
         
        Except where otherwise noted, content on this site is licensed under 
        a <a href="https://creativecommons.org/licenses/by/4.0/">
        Creative Commons Attribution 4.0 International</a> license.
         
        In order to start creating a Data Management Plan (DMP), you first need to create a "Project" with a project title, a brief description of your research question(s) or research field, and a catalog of questions for creating the DMP which you can select from a prepared list. Click on "Create new project" below to do so, or click on an already existing project name in the list below to access the project's information.
       About Answer questions Contact Create new project Creation Date Delete Project Edit Project Properties Fill in Questionaire GRO.data (research data repository) GRO.plan (data management planning) GRO.publications (publication data repository) Imprint Last updated My GRO.plan role My account Please enter each %(name)s separately by clicking on the green button, providing a shortname for the %(name)s and then entering the form. The different %(name_plural)s will be referred to in the following questions. You can edit or remove each %(name)s by clicking on the pencil or the garbage bin in the top right corner of each %(name)s page. Please provide a shortname or acronym for this %(name)s. Privacy Project Overview Project name Questionnaire for project <em>{$ service.project.title $}</em> Search project title Search projects Selected Catalog Show all projects Showing only projects with title matching  Terms and Conditions To whom it may concern, Update project views View all projects on %(site)s View answers View questions Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-03 18:08+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 
Um einen Datenmanagementplan (DMP) zu erstellen muss der Projekt-Fragebogen ausgefüllt werden. 
 Inhalte sind, sofern nicht anders gekennzeichnet, lizensiert unter <a href="https://creativecommons.org/licenses/by/4.0/"> Creative Commons Namensnennung 4.0 International</a>. 
Um einen Datenmanagementplan (DMP) zu erstellen, legen Sie zunächst ein "Projekt" an. Hierzu klicken Sie auf dieser Seite unten auf "Neues Projekt erstellen". Fügen Sie diesem einen Titel und eine kurze Beschreibung Ihrer Forschungsfrage(n) bzw. des Forschungsbereiches hinzu. Wählen Sie anschließend einen Fragenkatalog zur Erstellung des DMP aus der Liste aus.
</p><p>Um die Projektinformationen eines bereits angelegten Projektes einzusehen, klicken Sie einfach direkt auf dessen Namen in der nachstehenden Liste. Über GRO.plan Fragebogen ausfüllen Kontakt Neues Projekt erstellen Erstellt am Projekt löschen Projekteinstellungen bearbeiten Fragebogen ausfüllen GRO.data (Forschungsdatenrepositorium) GRO.plan (Datenmanagementpläne) GRO.publications (Publikationsdatenbank) Impressum Zuletzt bearbeitet Meine GRO.plan Rolle Mein Konto Bitte geben Sie alle %(name_plural)s einzeln ein, indem Sie ein Kürzel für %(name)s angeben. Die verschiedenen %(name_plural)s werden in den folgenden Fragen genutzt. Sie können %(name)s durch anklicken des Stiftes bearbeiten und löschen durch anklicken des Mülleimers in der rechten oberen Ecke jeder Seite. Bitte Kürzel oder Akronym für diesen %(name)s angeben. Datenschutzhinweis Projektübersicht Projektname Fragebogen für Projekt <em>{$ service.project.title $}</em> Suche nach Projekttitel Projekt suchen Ausgewählter Katalog Alle Projekte anzeigen Es werden nur Projekte angezeigt für Suchwort:  Nutzungsbedingungen Sehr geehrte Damen und Herren, Projekt Ansichten bearbeiten Alle Projekte auf %(site)s anzeigen Fragebogen ansehen Fragen anzeigen 