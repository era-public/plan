# The python base image and its version
ARG BASE_IMAGE=python:3.11-slim-bookworm

###
# The builder
###
FROM ${BASE_IMAGE} as builder

RUN apt-get update -y && apt-get upgrade -y && apt-get install --no-install-recommends -y \
    gettext \
    gcc \
    libc-dev \
    git \
    libpq-dev


COPY ./config /app/config
COPY ./gro_rdmo_theme /app/gro_rdmo_theme
COPY ./manage.py /app/manage.py
COPY ./requirements /app/requirements

WORKDIR /app

RUN mv /app/config/settings/docker.py /app/config/settings/local.py; \
    pip install --upgrade pip; \
    pip install -r requirements/production.txt

# initial catalogs
RUN git clone --depth 1 https://github.com/rdmorganiser/rdmo-catalog.git /rdmo-catalog


###
# The base image
###
FROM ${BASE_IMAGE} as base

RUN apt-get update -y && apt-get upgrade -y && apt-get install --no-install-recommends -y \
    libpq5 \
    postgresql-client \
    # gettext could move to dev image, if there is more to install there
    gettext \
    pandoc \
    texlive-xetex \
    lmodern \
    texlive-fonts-recommended \
    librsvg2-bin \
    # save some space
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local /usr/local
COPY --from=builder /app /app
COPY --from=builder /rdmo-catalog /rdmo-catalog

# maintenance scripts
COPY ./compose/django/maintenance /usr/local/bin/maintenance
RUN chmod +x /usr/local/bin/maintenance/*
RUN mv /usr/local/bin/maintenance/* /usr/local/bin \
    && rmdir /usr/local/bin/maintenance

COPY ./compose/django/entrypoint /entrypoint
RUN chmod +x /entrypoint

###
# The production image
###
FROM base as production
WORKDIR /app

COPY ./compose/django/start.production /start
RUN chmod +x /start

ENV DJANGO_SECRET_KEY=NOT_SO_SECRET_TEMP_KEY

RUN addgroup --system --gid 200 django \
    && adduser --system --uid 200 --ingroup django django \
    && chown -R django /app /entrypoint /start

USER django
ENTRYPOINT ["/entrypoint"]


###
# The local dev image
###
FROM base as local
WORKDIR /app

COPY ./compose/django/start /start
RUN chmod +x /start

ENV DJANGO_SECRET_KEY=NOT_SO_SECRET_TEMP_KEY

ENTRYPOINT ["/entrypoint"]
