# About

eRA  template modifications for the RDMO app - https://github.com/rdmorganiser/rdmo-app

[[_TOC_]]

# Running locally

This git repo is set up to be running with python virtual environments or with docker-compose.

## Local development and testing with virtual environment

Create virtual environment:

        python3 -m venv venv
        . venv/bin/activate

        pip install -r requirements/dev.txt

Create local sqlite database:

        ./manage.py migrate
        ./manage.py download_vendor_files

Run  development server:

        ./manage.py runserver
        
Afterwards the website is available at <http://localhost:8000>.

## Local development and testing with Docker

Build and run images:

        docker compose up --build

Afterwards the website is available at <http://localhost:8000>.

### Create superuser and import catalogs for local testing

On the first local run you have an empty database, you need to create a superuser
and import catalogs for testing.

Create superuser

        docker exec -it plan-django-1 python3 manage.py createsuperuser

To import catalogs look at <https://github.com/rdmorganiser/rdmo-catalog/blob/master/rdmorganiser/README.md>

The catalogs are already downloaded to `/rdmo-catalog/` so they may be imported like 

        docker exec plan-django-1 python3 manage.py import /rdmo-catalog/rdmorganiser/domain/rdmo.xml
        ...

There is a convenience script for the initial import [included](/compose/django/maintenance/import_catalogs) 
which could be called:

        docker exec plan-django-1 import_catalogs

### Empty the local postgres database

To empty the local postgres database you may just remove the container and its volumes:

        docker rm plan-postgres-1
        docker volume rm plan_local_postgres_data
        docker volume rm plan_local_postgres_data_backups

afterwards you need to create superuser and import catalogs again.


### Translation

If you work with strings and translations the following commands may be useful:

* Collect strings from modified templates:

        docker exec plan-django-1 python3 manage.py makemessages -l de -l en

* Compile changed messages:

        docker exec plan-django-1 python3 manage.py compilemessages -l de -l en

Strings collected by `makemessages` can be found in the folder [locale](./locale).
The folder [locale_override](./locale_override) has some strings from RDMO core,
which are not in the templates. These are in a separate directory to not 
be destroyed by `makemessages`. They are all compiled by `compilemessages` however.

# Deploy on production server

On a production server a different compose.yaml is used (compare with [puppet setup](https://gitlab.gwdg.de/era/gro-plan-puppet/-/blob/master/modules/era/templates/opt/compose/docker-compose.yaml.epp)).

Additional commands have to be run on first setup or on upgrades (compare with [RDMO documentation](https://rdmo.readthedocs.io/en/latest/installation/setup.html#basic-setup)):

        docker exec -it rdmo_django_1 python3 manage.py migrate

        docker exec -it rdmo_django_1 python3 manage.py setup_groups

        docker exec -it rdmo_django_1 python3 manage.py createsuperuser


## Postgres backup and restore

The PostgreSQL docker setup is copied from Cookiecutter Django, so this [documentation applies](https://cookiecutter-django.readthedocs.io/en/latest/docker-postgres-backups.html).

Backup:

        docker exec rdmo_postgres_1 backup

List backups:

        docker exec rdmo_postgres_1 backups

Restore:

        docker exec rdmo_postgres_1 restore backup_2020_04_03T23_29_10.sql.gz


# Postgres image

The container registry contains a docker image for postgres named `era/plan/postgres-image` which is built from branch `postgres-image`. For creating new image checkout that branch, merge develop, do your changes commit and push to gitlab. Do not forget to merge your changes back to develop branch afterwards.
